<?php
session_start();

$db = new PDO("sqlsrv:Server=DEV\SQL_DEV;Database=AXIMOSCOLCOM", "sa", "Apple91@Tk");
//$db = new PDO("sqlsrv:Server=APPSVR2;Database=AXIMOSCOLCOM", "aximos", "@ximos16%");

//$db = new PDO("sqlsrv:Server=DESKTOP-48BTLRK;Database=AXIMOSCOLCOM", "sa", "phillipkent8915#");
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

function get_distinct_routes(){
    global $db;
    try{
        $stmt = $db->prepare("select distinct(RouteID) from tblCustomers");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}
function GetCustomerID($CustomerNumber){
     global $db;
    try{
        $stmt = $db->prepare("select CustomerID from [AXIMOSCOLCOM].[dbo].[tblCustomers] where CustomerNumber =?");
        $stmt->execute(array($CustomerNumber));
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}

function getCustNum(){
     global $db;
    try{
        $stmt = $db->prepare("select distinct(CustomerNumber) from [AXIMOSCOLCOM].[dbo].[tblCustomers]");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}

function get_routes(){
    
     global $db;
    try{
        $stmt = $db->prepare("select * from tblRoutes");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}

function get_salesman(){
      global $db;
    try{
        $stmt = $db->prepare("select * from tblUsers where RoleID=3");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}


function get_routename($routeID){
    global $db;
    try{
        $stmt = $db->prepare("select RouteName from tblRoutes where RouteID=?");
        $stmt->execute(array($routeID));
        $result = $stmt->fetchColumn();
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result; 
}

function get_salesmn($userID){
    global $db;
    try{
        $stmt = $db->prepare("select Username from tblUsers where UserID=?");
        $stmt->execute(array($userID));
        $result = $stmt->fetchColumn();
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result; 
}



function get_customer_ID(){
      global $db;
    try{
        $stmt = $db->prepare("select distinct(CustomerID) from vwTransactionList_DebtorAge");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}

function get_customerID_givendate($datefrom,$dateto)
{
   global $db;
    try{
        $stmt = $db->prepare("select distinct(CustomerID) from vwTransactionList_DebtorAge where TransactionDate between ? and ?");
        $stmt->execute(array($datefrom,$dateto));
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;  
    
}

function getCustomerIDs(){
	 global $db;
    try{
        $stmt = $db->prepare("select distinct(CustomerID) from tblTriggeredTransactionsForAge where UseFlag is NULL");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;  
}

//print_r(get_customerID_givendate("2010-01-01","2016-10-10"));



function get_aged_customer_IDs($routeID){
      global $db;
    try{
        $stmt = $db->prepare("select distinct(CustomerID) from tblAgeSummary where CustomerID IN (select CustomerID from tblCustomers where StatusID !=2 AND RouteID=?)");
        $stmt->execute(array($routeID));
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}

function get_allaged_customer_IDs()
{
      global $db;
    try{
     
        $stmt = $db->prepare("select distinct(CustomerID) from tblAgeSummary where CustomerID IN (select CustomerID from tblCustomers where StatusID !=2)");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}


function get_salesman_customer_ids($salesmanID)
{
      global $db;
    try{
        $stmt = $db->prepare("select distinct(CustomerID) from tblAgeSummary where CustomerID IN (select CustomerID from tblCustomers where StatusID !=2 and RouteID IN (select RouteID from tblRoutes where DefaultDriverID=?))");
        $stmt->execute(array($salesmanID));
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}



function get_customer_details($customerID)
{
     global $db;
    try{
        $stmt = $db->prepare("select * from tblCustomers where CustomerID=? ");
        $stmt->execute(array($customerID));
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}


function initial_transactions_list($customerID,$datefrom,$dateto){
    global $db;
    try{
        $stmt = $db->prepare("select * from vwTransactionList_DebtorAge where CustomerID=? and TransactionDate between ? and ? order by TransactionDate ASC");
        $stmt->execute(array($customerID,$datefrom,$dateto));
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}

function get_transaction_type($ttypeID){
    global $db;
    try{
        $stmt = $db->prepare("select TransactionType from luCustomerTransactionTypes where CustomerTransactionTypeID=?");
        $stmt->execute(array($ttypeID));
        $result = $stmt->fetchColumn();
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;  
}

function get_current_aging($customerID){
     global $db;
    try{
        $stmt = $db->prepare("select * from tblAgeSummary where CustomerID=?");
        $stmt->execute(array($customerID));
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    } 
    return $result;
}

function Create_Age_Record($customerID,$customerName,$transactionType,$transactionRef,$lastTransDate,$lastRowNum,$OriginalTransactionAmount,$transAmnt,$totalOutstandingAmnt,$ReportStatus){
     global $db;
    try{
        $stmt = $db->prepare("insert into tblAgeSummary(CustomerID,CustomerName,TransactionType,TransactionRef,LastTransacationDate,LastTransRowNum,OriginalTransactionAmount,TransactionAmount,TotalOutstandingAmount,LastUpdateDate,ReportStatus) values(?,?,?,?,?,?,?,?,?,?,?)");
        $stmt->execute(array($customerID,$customerName,$transactionType,$transactionRef,$lastTransDate,$lastRowNum,$OriginalTransactionAmount,$transAmnt,$totalOutstandingAmnt,date("Y-m-d H:i:s"),$ReportStatus));
        $count = $stmt->rowCount();
        if($count>0){
            $result["status"]="ok";
            $result["id"]=$db->lastInsertId();
        }
        else{
            $result["status"]="fail";
        }
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    } 
    return $result;
}

function get_customer_lastinsert_trans($customerID){
      global $db;
    try{
        $stmt = $db->prepare("select TOP 1 * from tblAgeSummary where CustomerID = ? ORDER BY AgeID DESC");
        $stmt->execute(array($customerID));
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    } 
    return $result;
}



function get_all_transactions($customerID,$datefrom,$dateto,$lastRowNum,$lastinsertRef){
   global $db;
    try{
        $stmt = $db->prepare("select * from vwTransactionList_DebtorAge where CustomerID=? and TransactionDate between ? and ? and RowNumber>? and TransactionRef !=? order by RowNumber ASC");
        $stmt->execute(array($customerID,$datefrom,$dateto,$lastRowNum,$lastinsertRef));
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;  
}
//$datefrom = date("Y-m-d",  strtotime("04/01/2016"));
//$dateto = date("Y-m-d H:i:s");
//print_r(get_all_transactions(14,$datefrom,$dateto,$lastRowNum,$lastinsertRef));



function getCheckIfRefExists($TransactionRef){
     global $db;
    try{
        
        $stmt = $db->prepare("select count(*) as RefCounter from tblAgeSummary where TransactionRef = ?");
        $stmt->execute(array($TransactionRef));
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;  
}



function InvoicesToKnock($customerID,$receipt,$adjustment_neg,$lastInsertID){
    global $db;
    try{
        $report_status="Owing";
        $stmt = $db->prepare("select * from tblAgeSummary where CustomerID=? and (TransactionType!=? and TransactionType!=?) and AgeID<? and ReportStatus=? order by LastTransacationDate ASC");
        $stmt->execute(array($customerID,$receipt,$adjustment_neg,$lastInsertID,$report_status));
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;  
}

function getPaymentsToUse($customerID,$receipt,$adjustment_neg,$lastInsertID){
    global $db;
    try{
        $report_status="Unused";
        $stmt = $db->prepare("select * from tblAgeSummary where CustomerID=? and (TransactionType=? or TransactionType=?) and AgeID<? and ReportStatus=? order by LastTransacationDate ASC");
        $stmt->execute(array($customerID,$receipt,$adjustment_neg,$lastInsertID,$report_status));
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;  
}

function UpdateTransaction($transaction_Amnt,$report_status,$ageID){
  global $db;
    try{
        $stmt = $db->prepare("update tblAgeSummary set TransactionAmount=?, ReportStatus=? where AgeID=?");
        $stmt->execute(array($transaction_Amnt,$report_status,$ageID));
        $count = $stmt->rowCount();
        if($count>0){
            $result["status"]="ok";
           
        }
        else{
            $result["status"]="fail";
        }
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    } 
    return $result;  
}

function customer_aging($customerID,$dateto){
  global $db;
    try{
        
        $stmt = $db->prepare("select * from tblAgeSummary where CustomerID=? and CONVERT(date, LastTransacationDate) <= ? order by LastTransRowNum desc");
        $stmt->execute(array($customerID,$dateto));
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    } 
    return $result;   
}

//print_r(customer_aging(275,"2017-08-26"));

function CustomerAgingCurrent($customerID){
    global $db;
    try{
        $report_status_pay = "Unused";
        $report_status_Inv = "Owing";
        $stmt = $db->prepare("select * from tblAgeSummary where CustomerID=? and (ReportStatus=? or ReportStatus=?)");
        $stmt->execute(array($customerID,$report_status_pay,$report_status_Inv));
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    } 
    return $result; 
}




function get_max_transNum($tblname,$fieldname,$transactionDate,$datefrom,$dateto)
{
     global $db;
    try{
        
        $stmt = $db->prepare("select $fieldname as max_row from $tblname where $transactionDate between ? and ? order by $fieldname desc");
        $stmt->execute(array($datefrom,$dateto));
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    } 
    return $result;   
}

function get_max_transRowNum($tblname,$fieldname,$CustomerIDField,$CustomerID)
{
     global $db;
    try{
        
        $stmt = $db->prepare("select $fieldname as max_row from $tblname where $CustomerIDField = ?  order by $fieldname desc");
        $stmt->execute(array($CustomerID));
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    } 
    return $result;   
}




function delete_age_rec(){
    global $db;
    try{
        
        $stmt = $db->prepare("delete from tblAgeSummary");
		$reset_ID = $db->prepare("DBCC CHECKIDENT (tblAgeSummary,RESEED,0)");
		$reset_ID->execute();
        $stmt->execute();
        $result = "done";
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    } 
    return $result;   
}


function DeleteCustomerAgeRecord($CustomerID){
    global $db;
    try{
        
        $stmt = $db->prepare("delete from tblAgeSummary where CustomerID=?");
	$stmt->execute(array($CustomerID));
        $result = "done";
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    } 
    return $result;   
}

function last_date($CustomerID){
      global $db;
    try{
        $stmt = $db->prepare("select * FROM [AXIMOSCOLCOM].[dbo].[tblAgeSummary] where CustomerID = ? order by LastTransacationDate desc");
        $stmt->execute(array($CustomerID));
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    } 
    return $result;
}

function last_transaction()
{

}

function sp_upload_invoices(){
    global $db;
    try{
        
        $stmt = $db->prepare("EXEC sp_UploadAllCreditInvoices");
	$stmt->execute();
        $result = "done";
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    } 
    return $result;   
}

function sp_upload_receipts(){
    global $db;
    try{
        
        $stmt = $db->prepare("EXEC sp_uploadReceipts");
	$stmt->execute();
        $result = "done";
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    } 
    return $result;   
}


function sp_upload_journals(){
    global $db;
    try{
        
        $stmt = $db->prepare("EXEC sp_uploadJournals");
	$stmt->execute();
        $result = "done";
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    } 
    return $result;   
}

function get_transactions_to_age($customerID)
{
    global $db;
    try{
        
        $stmt = $db->prepare("select * from tblTriggeredTransactionsForAge where CustomerID=? and UseFlag IS NULL order by id asc");
        $stmt->execute(array($customerID));
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;  
}


function update_usage_flag($source_table_id)
{
    global $db;
    try{
        $stmt = $db->prepare("update tblTriggeredTransactionsForAge set UseFlag=1 where id = ?");
        $stmt->execute(array($source_table_id));
        $count = $stmt->rowCount();
        if($count>0){
            $result["status"]="ok";
           
        }
        else{
            $result["status"]="fail";
        }
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    } 
    return $result;     
}


/*
delete from tblTriggeredTransactionsForAge
     DBCC CHECKIDENT (tblTriggeredTransactionsForAge, RESEED,0) 

	  delete from tblAgeSummary
     DBCC CHECKIDENT (tblAgeSummary, RESEED,0) 
     */


