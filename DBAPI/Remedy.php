<?php 

//include("dbapi.php");


$customers = $_POST["custs"];

if(is_array($customers)==true){
   
}else{
      $customers = explode(",",$customers);
   // print_r($customers);
}

foreach($customers as $CustomerNumber){
    $CstData = GetCustomerID($CustomerNumber);
    $customerID = $CstData[0]["CustomerID"];
    DeleteCustomerAgeRecord($customerID);
    
    $Data = last_date($customerID);
  $datefrom = date("Y-m-d",  strtotime("04/01/2016"));
$dateto = date("Y-m-d H:i:s");
$ageDateFrom =  date("Y-m-d",  strtotime("04/01/2016"));
$ageDateTo = date("Y-m-d H:i:s");  
 //check if date set is les that the date requireds

$trans = sizeof(initial_transactions_list($customerID,$datefrom,$dateto));
// 
for($i=0;$i<$trans;$i++){
$get_curr_age = get_current_aging($customerID);
if(empty($get_curr_age)){
   $newdata = initial_transactions_list($customerID,$datefrom,$dateto);
   $customerID = $newdata[0]["CustomerID"];
   $customerName = $newdata[0]["CustomerName"];
   $transactionType = get_transaction_type($newdata[0]["TransactionTypeID"]);
   $transactionRef = $newdata[0]["TransactionRef"];
   $lastTransDate = $newdata[0]["TransactionDate"];
   $lastRowNum = $newdata[0]["RowNumber"];
   $transAmnt = abs($newdata[0]["Amount"]);
   
      if($transactionType=="Invoice" || $transactionType=="Take On Balance" || $transactionType=="Adjustment +"){
      $ReportStatus = "Owing";
      $totalOutstandingAmnt = $transAmnt;
    }
    elseif($transactionType=="Receipt" || $transactionType=="Adjustment -"){
     $ReportStatus = "Unused";   
     $totalOutstandingAmnt = -$transAmnt;
    }

   
 $create_new_age = Create_Age_Record($customerID,$customerName,$transactionType,$transactionRef,$lastTransDate,$lastRowNum,$transAmnt,$transAmnt,$totalOutstandingAmnt,$ReportStatus);
  if($create_new_age["status"]=="ok"){
   // echo "New age record set";
   
}
else{
   // echo "error: ".$create_new_age["status"];
 
}
 
}
else{               //if value 
                    // get last insert value
    $last_trans_set = get_customer_lastinsert_trans($customerID);
    $lastDateFormatted  = date("Y-m-d H:i:s",strtotime($last_trans_set[0]['LastTransacationDate']));
$last_amount = $last_trans_set[0]["TotalOutstandingAmount"];
$last_transaction_ref = $last_trans_set[0]["TransactionRef"];
$lastRowNum = $last_trans_set[0]["LastTransRowNum"];

//get all transactions after the last date
$alltransactions = get_all_transactions($customerID,$lastDateFormatted,$dateto,$lastRowNum,$last_transaction_ref);

if(!empty($alltransactions)){
      $transaction_Date = $alltransactions[0]['TransactionDate'];
    $customerID =  $alltransactions[0]['CustomerID'];
   $customerName = $alltransactions[0]['CustomerName'];
   $transaction_type = get_transaction_type($alltransactions[0]['TransactionTypeID']);
   $amount = abs($alltransactions[0]['Amount']);
   $transaction_ref = $alltransactions[0]['TransactionRef'];
   $row_num = $alltransactions[0]["RowNumber"];
  
    
    // if it is an invoice, add to the debtor's account so is the adjustment + and the take on
 if($transaction_type=="Invoice" || $transaction_type=="Take On Balance" || $transaction_type=="Adjustment +"){
      $ReportStatus = "Owing";
        $outstanding_amount = $last_amount+$amount;
    //check if there is a payment still hanging
    // knock off the invoice and then kill the payment if the balance 
   $add_new_age = Create_Age_Record($customerID,$customerName,$transaction_type,$transaction_ref,$transaction_Date,$row_num,$amount,$amount,$outstanding_amount,$ReportStatus);
   if($add_new_age["status"]=="ok"){
       $lastID = $add_new_age["id"];
       $receipts = getPaymentsToUse($customerID,"Receipt","Adjustment -",$lastID);
       if(!empty($receipts)){
           $inv_amount = $amount;
           foreach($receipts as $rec){
               $ageID = $rec["AgeID"];
           $rec_amount = $rec["TransactionAmount"];
           
           if($inv_amount>$rec_amount){
               $inv_amount = $inv_amount-$rec_amount;
               $rec_amount=0;
               $pay_report_status = "Used";
               $inv_report_status = "Owing";
              
           }elseif($inv_amount<$rec_amount){
               $rec_amount = $rec_amount - $inv_amount;
               $inv_amount = 0;
                $pay_report_status = "Unused";
               $inv_report_status = "Paid";
               
               
           }elseif($inv_amount==$rec_amount){
               $inv_amount=0;
               $rec_amount=0;
               $pay_report_status = "Used";
               $inv_report_status = "Paid";
           }
           
             $update_inv = UpdateTransaction(round($inv_amount,4),$inv_report_status,$lastID); //update invoice
              $update_payment = UpdateTransaction(round($rec_amount,4),$pay_report_status,$ageID); //update payment
              if($update_inv["status"]=="ok" && $update_payment["status"]=="ok")
              {
                 // echo "payment and inv updated";
              }
              else{
                /* echo "<br>error: ".$update_inv["status"].$update_payment["status"];
                  echo "<br> kana iri invoice amaount (invoices) : ".$inv_amount;
                  echo "<br> kana iri paid amount  (invoices) : ".$rec_amount."<br>";
                 * 
                 */
                  
                  
              }
           
           }
       }

   }else{
       //echo $add_new_age["status"];
   }
 }
 // here knock off the balances based on FIFO of the entries in the age report - get the array and knock the figures off
  elseif($transaction_type=="Receipt" || $transaction_type=="Adjustment -"){
     $ReportStatus = "Unused";  
       $outstanding_amount = $last_amount-$amount;
    $add_new_age = Create_Age_Record($customerID,$customerName,$transaction_type,$transaction_ref,$transaction_Date,$row_num,$amount,$amount,$outstanding_amount,$ReportStatus);
     if($add_new_age["status"]=="ok"){ //knock off the initial invoices
       $last_insert_paymentID = $add_new_age["id"];
      $get_fifo_invoices = InvoicesToKnock($customerID,"Receipt","Adjustment -",$last_insert_paymentID);
      if(!empty($get_fifo_invoices)){ // knck off
          // get current payment balance - 
          $rec_amount = $amount;
       foreach($get_fifo_invoices as $adj_inv){
           $ageID = $adj_inv["AgeID"];
           $inv_amount = $adj_inv["TransactionAmount"];
           if($inv_amount>$rec_amount){ //inv amount > payment
               $inv_amount = $inv_amount-$rec_amount;
               $rec_amount = 0;
               $inv_rep_state = "Owing";
               $Payment_report_status = "Used";
              
           }elseif($inv_amount<$rec_amount){ // inv less that payment
               $rec_amount = $rec_amount-$inv_amount;
               $inv_amount = 0;
               $inv_rep_state = "Paid";
               $Payment_report_status = "Unused";
              
           }elseif($inv_amount==$rec_amount){
               $inv_amount = 0;
               $rec_amount = 0;
                $inv_rep_state = "Paid";
               $Payment_report_status = "Used";
              
           
       }
       $update_inv = UpdateTransaction(round($inv_amount,4),$inv_rep_state,$ageID); //update invoice
              $update_payment = UpdateTransaction(round($rec_amount,4),$Payment_report_status,$last_insert_paymentID); //update payment
              if($update_inv["status"]=="ok" && $update_payment["status"]=="ok")
              {
                 // echo "payment and inv done";
              }
              else{
              /*   echo "error: ".$update_inv["status"].$update_payment["status"];
                   
                  echo "<br> kana iri invoice amaount if (receipts) : ".$inv_amount;
                  echo "<br> kana iri paid amount  (receipts) ".$rec_amount."<br>";
               * 
               */
                 
              }
      }
   }
    }
   else{
       //echo $add_new_age["status"];
   } 
    
 

}
}
else{
  //echo "no more records after date ".$lastDateFormatted." and after ref ".$last_transaction_ref;
break;
}
}

}
}//end of customers for each

echo "Exceptions successfully handled, pease create your age as usual.";
