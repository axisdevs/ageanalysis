SELECT        T.DeliveryDate AS TransactionDate, T.RouteSheetDetailID AS TransactionID, T.InvoiceNumber AS TransactionRef, 4 AS TransactionTypeID, T.TransactionAmount AS Amount, T.CustomerID, C.CustomerName, C.CustomerNumber, 
                         'Invoice ' AS Notes
FROM 
           
(SELECT  RouteSheetDetailID, CustomerID, RouteSheetHeaderID, MarketSize, PlainTimeIn, DeliveryDate, ActualTimeIn, Mileage, FirstOrder, TopUpTime, SecondOrder, [Returns], ReaturnReason, CratesIssued, 
                                                    CratesCollected, CratesIssuedTotal, CratesCollectedTotal, Total, TotalVAT, CashShortfall, Comment, Syncronised, FiscalDocumentPrinted, InvoiceNumber, NumberOfItemsDelivered, NumberOfItemsReturned, 
                                                    ItemsDeliveredTotal, ItemsReturnedTotal, ReceivedBy, InvoiceTypeID, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy, DeviceID, PaymentMethod, Source, VisitStatusID, BranchID, RowUniqueID, GPSLocation, 
                                                    ReferenceNumber, UploadedToERP, AmountTendered, CustomerPaymentDiscountAmount, ErrorCount, ErrorMessages, HasErrors, AllowSync, CUSTOMER_ROWGUID, GPSLatitude, GPSLongitude, InvoicePrinted, 
                                                    DeliveryRef, UplaodedToAthority, Change, LinkedInvoiceNumber, LinkedTransactionGUID, SalesOrderNumber, SalesOrderNumberGUID, SalesSessionGUID, CurrencyChangeAmount, CurrencyExchangeBuyRate, 
                                                    CurrencyExchangeSellRate, TenderCurrencyID, TotalCurrencyConvertedValue, TenderedComboBond, TenderedComboCash, TenderedComboEcoCash, TenderedComboPOS,
                                                        (SELECT        SUM(Total) AS Expr1
                                                          FROM            dbo.tblInvoiceDetails
                                                          WHERE        (RouteSheetDetailID = I.RouteSheetDetailID)) AS TransactionAmount
                          FROM            dbo.tblRouteSheetDetails AS I
                          WHERE        (VisitStatusID = 2) AND (PaymentMethod IN ('Account', 'TP'))) AS T INNER JOIN
                         dbo.tblCustomers AS C ON C.CustomerID = T.CustomerID