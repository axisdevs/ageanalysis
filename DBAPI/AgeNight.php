<?php 

include("dbapi.php");
require("../MailServices/class.phpmailer.php");
require("../MailServices/Maiiler.php");

//delete_age_rec();

//Mail parameters


 
//upload stored procedure changes
sp_upload_invoices();
sp_upload_receipts();
sp_upload_journals();


$datefrom = date("Y-m-d H:i:s",strtotime("01/01/16"));
$dateto = date("Y-m-d H:i:s");
$customers = getCustomerIDs();
$counteruni = 1;


//Mail parameters
 $message = "Dear Team <br> Kindly note the age analysis has started its run on $dateto";
 // use wordwrap() if lines are longer than 70 characters
 $msg = wordwrap($message, 70);
 
 $subject = "Age Analysis";
 $SubjectHeaderName = "";
 $email = 'tmadzamba@axissol.com';

  $ScheduleMailer = SendMail($SubjectHeaderName, $subject, $message, $email);



foreach($customers as $CustomerNumber){  

        $customerID = $CustomerNumber["CustomerID"];
        
   //$tranVwList = get_max_transRowNum("vwTransactionList_DebtorAge","RowNumber","CustomerID",$customerID);
   $trans_to_age = get_transactions_to_age($customerID);

//$transDebtorAge = get_max_transRowNum("tblAgeSummary","LastTransRowNum","CustomerID",$customerID);
    
if(count($trans_to_age) == 0){
    echo "<br>No More transactions for Customer $customerID ";
    continue;
}
 

$trans = sizeof($trans_to_age);
// 
for($i=0;$i<$trans;$i++){
         
$alltransactions = $trans_to_age;

if(!empty($alltransactions)){
      $transaction_Date = $alltransactions[$i]['TransactionDate'];
    $customerID =  $alltransactions[$i]['CustomerID'];
   //$transaction_type = get_transaction_type($alltransactions[$i]['TransactionTypeID']);
   $transaction_type = $alltransactions[$i]['TransType'];
   $amount = abs($alltransactions[$i]['TransactionAmount']);
   $transaction_ref = $alltransactions[$i]['TransactionRef'];
   $row_num = $alltransactions[$i]["id"];

   $customer_info = get_customer_details($customerID);
   $customerName = $customer_info[0]['CustomerName'];
  
   //get customer ID last outstanding amount in every loop
   $last_trans = get_customer_lastinsert_trans($customerID);
   if(count($last_trans) > 0)
   {
    $last_amount= $last_trans[0]['TotalOutstandingAmount'];
   }
   else
   {
    $last_amount = 0;
   }
 
    
    // if it is an invoice, add to the debtor's account so is the adjustment + and the take on
 if($transaction_type=="Invoice" || $transaction_type=="Take On Balance" || $transaction_type=="Adjustment +"){
      $ReportStatus = "Owing";
        $outstanding_amount = $last_amount+$amount;
    //check if there is a payment still hanging
    // knock off the invoice and then kill the payment if the balance 
   $add_new_age = Create_Age_Record($customerID,$customerName,$transaction_type,$transaction_ref,$transaction_Date,$row_num,$amount,$amount,$outstanding_amount,$ReportStatus);
   if($add_new_age["status"]=="ok"){
       //update transaction as used 
       update_usage_flag($row_num);

       $lastID = $add_new_age["id"];

       $receipts = getPaymentsToUse($customerID,"Receipt","Adjustment -",$lastID);
       if(!empty($receipts)){
           $inv_amount = $amount;
           foreach($receipts as $rec){
               $ageID = $rec["AgeID"];
           $rec_amount = $rec["TransactionAmount"];
           $row_id = $rec['LastTransRowNum'];
           
           if($inv_amount>$rec_amount){
               $inv_amount = $inv_amount-$rec_amount;
               $rec_amount=0;
               $pay_report_status = "Used";
               $inv_report_status = "Owing";
               
              
           }elseif($inv_amount<$rec_amount){
               $rec_amount = $rec_amount - $inv_amount;
               $inv_amount = 0;
                $pay_report_status = "Unused";
               $inv_report_status = "Paid";
               
               
           }elseif($inv_amount==$rec_amount){
               $inv_amount=0;
               $rec_amount=0;
               $pay_report_status = "Used";
               $inv_report_status = "Paid";
           }
           
             $update_inv = UpdateTransaction(round($inv_amount,4),$inv_report_status,$lastID); //update invoice
              $update_payment = UpdateTransaction(round($rec_amount,4),$pay_report_status,$ageID); //update payment




              if($update_inv["status"]=="ok" && $update_payment["status"]=="ok")
              {
                update_usage_flag($row_id);
                  echo "<br>payment and inv updated";
              }
              else{
                 echo "<br>error: ".$update_inv["status"].$update_payment["status"];
                  echo "<br> kana iri invoice amaount (invoices) : ".$inv_amount;
                  echo "<br> kana iri paid amount  (invoices) : ".$rec_amount."<br>";
                  
                 
                  
                  
              }
           
           }
       }

   }else{
       echo $add_new_age["status"];
   }
 }
 // here knock off the balances based on FIFO of the entries in the age report - get the array and knock the figures off
  elseif($transaction_type=="Receipt" || $transaction_type=="Adjustment -"){
     $ReportStatus = "Unused";  
       $outstanding_amount = $last_amount-$amount;
    $add_new_age = Create_Age_Record($customerID,$customerName,$transaction_type,$transaction_ref,$transaction_Date,$row_num,$amount,$amount,$outstanding_amount,$ReportStatus);
     if($add_new_age["status"]=="ok"){ //knock off the initial invoices
        update_usage_flag($row_num);
       $last_insert_paymentID = $add_new_age["id"];
      $get_fifo_invoices = InvoicesToKnock($customerID,"Receipt","Adjustment -",$last_insert_paymentID);
      if(!empty($get_fifo_invoices)){ // knck off
          // get current payment balance - 
          $rec_amount = $amount;
       foreach($get_fifo_invoices as $adj_inv){
           $ageID = $adj_inv["AgeID"];
           $inv_amount = $adj_inv["TransactionAmount"];
           $source_id = $adj_inv["LastTransRowNum"];
           if($inv_amount>$rec_amount){ //inv amount > payment
               $inv_amount = $inv_amount-$rec_amount;
               $rec_amount = 0;
               $inv_rep_state = "Owing";
               $Payment_report_status = "Used";
              
           }elseif($inv_amount<$rec_amount){ // inv less that payment
               $rec_amount = $rec_amount-$inv_amount;
               $inv_amount = 0;
               $inv_rep_state = "Paid";
               $Payment_report_status = "Unused";
              
           }elseif($inv_amount==$rec_amount){
               $inv_amount = 0;
               $rec_amount = 0;
                $inv_rep_state = "Paid";
               $Payment_report_status = "Used";
              
           
       }
       $update_inv = UpdateTransaction(round($inv_amount,4),$inv_rep_state,$ageID); //update invoice
              $update_payment = UpdateTransaction(round($rec_amount,4),$Payment_report_status,$last_insert_paymentID); //update payment
              if($update_inv["status"]=="ok" && $update_payment["status"]=="ok")
              {
                update_usage_flag($source_id);
                  echo "payment and inv done";
              }
              else{
                echo "error: ".$update_inv["status"].$update_payment["status"];
                   
                  echo "<br> kana iri invoice amaount if (receipts) : ".$inv_amount;
                  echo "<br> kana iri paid amount  (receipts) ".$rec_amount."<br>";
                
                                
              }
      }
   }
    }
   else{
       echo $add_new_age["status"];
   } 
    
 

}
}
else{
  echo "no more records after date ".$lastDateFormatted." and after ref ".$last_transaction_ref;
break;
}


}
  $resp = "-------------------------- C_ID:".$customerID." # ".$counteruni." of ".sizeof($customers)." @ ".date("Y-m-d H:i:s")." --- ---------------------------------------";
    echo '<br>'.$resp;
	
	$counteruni++;

}//end of customers for each

//sendmail

