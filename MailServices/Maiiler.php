<?php

function SendMail($SubjectHeaderName,$subject,$message,$email)
{
$mail = new PHPMailer;
$mail->SetLanguage( 'en', 'phpmailer/language/' );
$mail->isSMTP();
$mail->SMTPDebug = false;
$mail->Host = "smtp.gmail.com";
$mail->SMTPAuth = true;

// But you can comment from here
$mail->SMTPSecure = "tls";
$mail->Port = 587;
$mail->CharSet = "UTF-8";
// To here. 'cause default secure is TLS.

$mail->setFrom("", $SubjectHeaderName);
$mail->Username = "";
$mail->Password = "";

$mail->Subject = $subject;
$mail->msgHTML($message);


$mail->addAddress($email);
$sendMail = $mail->Send();
return $sendMail;
}

