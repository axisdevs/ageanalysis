<?php
require '../DBAPI/dbapi.php';
$salesman = $_POST["salesman"];

$date_to = date("Y-m-d",strtotime($_POST["date_to"]));
$rutz = "ALL";

if($date_to==date("Y-m-d")){
     $date_to = date("Y-m-d H:i:s");
 }
 elseif($date_to==date("1970-01-01")){
     $date_to = date("Y-m-d H:i:s");
 }



$salesmanArry = array();
if(@$salesman[0]==$rutz  || empty($salesman)){
    $all_sman = get_salesman();
    foreach($all_sman as $slsmn){
    $SalesmanID = $slsmn["UserID"];
    array_push($salesmanArry, $SalesmanID);
    }
    $salesman  = $salesmanArry;
}

// get the transactions to age summary


// Code logic begins here //
//CHECK if the age db has data
// if no, create first record 
// else Create a loop 
//  get the last insert value and and insert next
// if next is empty break the loop
$datefrom = date("Y-m-d",  strtotime("04/01/2016"));
$dateto = $date_to;
$ageDateFrom =  date("Y-m-d",  strtotime("04/01/2016"));
$ageDateTo = date("Y-m-d H:i:s");
$transListRn = get_max_transNum("vwTransactionList_DebtorAge","RowNumber","TransactionDate",$datefrom,$dateto);
$ageRn = get_max_transNum("tblAgeSummary","LastTransRowNum","LastTransacationDate",$ageDateFrom,$ageDateTo);



if(@$transListRn[0]["max_row"]==@$ageRn[0]["max_row"]) // this checks if the last age band is still the same
{
   // echo "No AGING, AGE REPORT UP TO DATE";
    
    
}

else{

	
$all_customers = get_customerID_givendate($datefrom,$dateto);

foreach($all_customers as $cust){  // age per customer 
    $customerID = $cust["CustomerID"];
    $Data = last_date($customerID);
    
@$LastInsertDate =  $Data[0]["LastTransacationDate"];

if($LastInsertDate<$dateto){ //check if date set is les that the date requireds

$trans = sizeof(initial_transactions_list($customerID,$datefrom,$dateto));
// 
for($i=0;$i<$trans;$i++){
$get_curr_age = get_current_aging($customerID);
if(empty($get_curr_age)){
   $newdata = initial_transactions_list($customerID,$datefrom,$dateto);
   $customerID = $newdata[0]["CustomerID"];
   $customerName = $newdata[0]["CustomerName"];
   $transactionType = get_transaction_type($newdata[0]["TransactionTypeID"]);
   $transactionRef = $newdata[0]["TransactionRef"];
   $lastTransDate = $newdata[0]["TransactionDate"];
   $lastRowNum = $newdata[0]["RowNumber"];
   $transAmnt = abs($newdata[0]["Amount"]);
   
      if($transactionType=="Invoice" || $transactionType=="Take On Balance" || $transactionType=="Adjustment +"){
      $ReportStatus = "Owing";
      $totalOutstandingAmnt = $transAmnt;
    }
    elseif($transactionType=="Receipt" || $transactionType=="Adjustment -"){
     $ReportStatus = "Unused";   
     $totalOutstandingAmnt = -$transAmnt;
    }

   
 $create_new_age = Create_Age_Record($customerID,$customerName,$transactionType,$transactionRef,$lastTransDate,$lastRowNum,$transAmnt,$transAmnt,$totalOutstandingAmnt,$ReportStatus);
  if($create_new_age["status"]=="ok"){
   // echo "New age record set";
   
}
else{
  //  echo "error: ".$create_new_age["status"];
 
}
 
}
else{               //if value 
                    // get last insert value
    $last_trans_set = get_customer_lastinsert_trans($customerID);
    $lastDateFormatted  = date("Y-m-d H:i:s",strtotime($last_trans_set[0]['LastTransacationDate']));
$last_amount = $last_trans_set[0]["TotalOutstandingAmount"];
$last_transaction_ref = $last_trans_set[0]["TransactionRef"];
$lastRowNum = $last_trans_set[0]["LastTransRowNum"];
//get all transactions after the last date
$alltransactions = get_all_transactions($customerID,$lastDateFormatted,$dateto,$lastRowNum,$last_transaction_ref);

if(!empty($alltransactions)){
    
      $transaction_Date = $alltransactions[0]['TransactionDate'];
    $customerID =  $alltransactions[0]['CustomerID'];
   $customerName = $alltransactions[0]['CustomerName'];
   $transaction_type = get_transaction_type($alltransactions[0]['TransactionTypeID']);
   $amount = abs($alltransactions[0]['Amount']);
   $transaction_ref = $alltransactions[0]['TransactionRef'];
   $row_num = $alltransactions[0]["RowNumber"];
  
   
 if($transaction_type=="Invoice" || $transaction_type=="Take On Balance" || $transaction_type=="Adjustment +"){
      $ReportStatus = "Owing";
        $outstanding_amount = $last_amount+$amount;
    //check if there is a payment still hanging
    // knock off the invoice and then kill the payment if the balance 
   $add_new_age = Create_Age_Record($customerID,$customerName,$transaction_type,$transaction_ref,$transaction_Date,$row_num,$amount,$amount,$outstanding_amount,$ReportStatus);
   if($add_new_age["status"]=="ok"){
       $lastID = $add_new_age["id"];
       $receipts = getPaymentsToUse($customerID,"Receipt","Adjustment -",$lastID);
       if(!empty($receipts)){
           $inv_amount = $amount;
           foreach($receipts as $rec){
               $ageID = $rec["AgeID"];
           $rec_amount = $rec["TransactionAmount"];
           
           if($inv_amount>$rec_amount){
               $inv_amount = $inv_amount-$rec_amount;
               $rec_amount=0;
               $pay_report_status = "Used";
               $inv_report_status = "Owing";
              
           }elseif($inv_amount<$rec_amount){
               $rec_amount = $rec_amount - $inv_amount;
               $inv_amount = 0;
                $pay_report_status = "Unused";
               $inv_report_status = "Paid";
               
               
           }elseif($inv_amount==$rec_amount){
               $inv_amount=0;
               $rec_amount=0;
               $pay_report_status = "Used";
               $inv_report_status = "Paid";
           }
           
             $update_inv = UpdateTransaction(round($inv_amount,4),$inv_report_status,$lastID); //update invoice
              $update_payment = UpdateTransaction(round($rec_amount,4),$pay_report_status,$ageID); //update payment
              if($update_inv["status"]=="ok" && $update_payment["status"]=="ok")
              {
                 // echo "payment and inv updated";
              }
              else{
                 /* echo "<br>error: ".$update_inv["status"].$update_payment["status"];
                  echo "<br> kana iri invoice amaount (invoices) : ".$inv_amount;
                  echo "<br> kana iri paid amount  (invoices) : ".$rec_amount."<br>";
                  * 
                  */
                  
              }
           
           }
       }

   }else{
       //echo $add_new_age["status"];
   }
 }
 // here knock off the balances based on FIFO of the entries in the age report - get the array and knock the figures off
  elseif($transaction_type=="Receipt" || $transaction_type=="Adjustment -"){
     $ReportStatus = "Unused";  
       $outstanding_amount = $last_amount-$amount;
    $add_new_age = Create_Age_Record($customerID,$customerName,$transaction_type,$transaction_ref,$transaction_Date,$row_num,$amount,$amount,$outstanding_amount,$ReportStatus);
     if($add_new_age["status"]=="ok"){ //knock off the initial invoices
       $last_insert_paymentID = $add_new_age["id"];
      $get_fifo_invoices = InvoicesToKnock($customerID,"Receipt","Adjustment -",$last_insert_paymentID);
      if(!empty($get_fifo_invoices)){ // knck off
          // get current payment balance - 
          $rec_amount = $amount;
       foreach($get_fifo_invoices as $adj_inv){
           $ageID = $adj_inv["AgeID"];
           $inv_amount = $adj_inv["TransactionAmount"];
           if($inv_amount>$rec_amount){ //inv amount > payment
               $inv_amount = $inv_amount-$rec_amount;
               $rec_amount = 0;
               $inv_rep_state = "Owing";
               $Payment_report_status = "Used";
              
           }elseif($inv_amount<$rec_amount){ // inv less that payment
               $rec_amount = $rec_amount-$inv_amount;
               $inv_amount = 0;
               $inv_rep_state = "Paid";
               $Payment_report_status = "Unused";
              
           }elseif($inv_amount==$rec_amount){
               $inv_amount = 0;
               $rec_amount = 0;
                $inv_rep_state = "Paid";
               $Payment_report_status = "Used";
              
           
       }
       $update_inv = UpdateTransaction(round($inv_amount,4),$inv_rep_state,$ageID); //update invoice
              $update_payment = UpdateTransaction(round($rec_amount,4),$Payment_report_status,$last_insert_paymentID); //update payment
              if($update_inv["status"]=="ok" && $update_payment["status"]=="ok")
              {
                 // echo "payment and inv done";
              }
              else{
                /* echo "error: ".$update_inv["status"].$update_payment["status"];
                   
                  echo "<br> kana iri invoice amaount if (receipts) : ".$inv_amount;
                  echo "<br> kana iri paid amount  (receipts) ".$rec_amount."<br>";
                 * 
                 */
              }
      }
   }
    }
   else{
     //  echo $add_new_age["status"];
   } 
    
 

}
//} //end of check if not in the record
}
else{
  //echo "no more records after date ".$lastDateFormatted." and after ref ".$last_transaction_ref;
break;
}
}

}
}//get transactions for that set date
else{
    //echo "date out of range";
}
}//end of each customer age loop
}


/***************************display starts here ********************/

$band = $_POST["bands"];
 for($i=0;$i<=7;$i++)
 {
     $bandval[$i] = $band*$i;
 }
 echo '<div class="block-header table-responsive ">
      <table class="table table-vcenter col-xs-12 col-md-12 col-lg-12 mytable">
      <tr>
</tr>
      <tbody>
     <tr style="font-size:13px">
     <td style="color:green;">
    <strong> Age by Salesman as at <span class="badge badge-primary">'.$date_to.'</span> <br>

</td></tr>   
</tbody>
<tr>
</tr>

</table>
</div>';

 foreach($salesman as $slsmnid){
     
$sales_name = get_salesmn($slsmnid);

  echo '<div class="block-header table-responsive ">
           
 <table class="table table-vcenter col-xs-12 col-md-12 col-lg-12 mytable" id="thistab">
 <tr >
</tr>
      <tbody>
     <tr style="font-size:13px">
     <td>
       <strong>Salesman  : '.$sales_name.' </strong>
</td></tr>   
</tbody>
<tr>
</tr>

</table>
     </div>

           
   <div class="block-content">
          
            <div class="table-responsive ">
                <table class="table table-vcenter col-xs-12 col-md-12 col-lg-12">
                 
                   <tbody>
                     <tr style="font-size:13px">
                         <td class="text-left " style="width:6%; text-transform: lowercase;"><strong>Acc #<strong></td>
                        <td class="text-left" style="width: 14%; text-transform: lowercase;"><strong>Customer</td>
                        <td class="text-right" style="width: 7%; text-transform: lowercase;"><strong>Cr Lim</td>
                        <td class=" text-right" style="width: 10%;text-transform: lowercase;"><strong>Balance</td>
                        <td class=" text-right" style="width: 9%;text-transform: lowercase;"><strong>'.$bandval[1].' Days</td>
                        <td  class=" text-right" style="width: 9%;text-transform: lowercase;" ><strong>'.$bandval[2].' Days</td>
                         <td class=" text-right" style="width: 9%;text-transform: lowercase;" ><strong>'.$bandval[3].' Days</td>
                        <td class="text-right" style="width: 9%;text-transform: lowercase;"><strong>'.$bandval[4].' Days</td>
                        <td class="text-right" style="width: 9%;text-transform: lowercase;"><strong>'.$bandval[5].' Days</td>
                        <td class="text-right" style="width: 9%;text-transform: lowercase;"><strong>'.$bandval[6].' Days</td>
                         <td class="text-right" style="width: 9%;text-transform: lowercase;"><strong>'.$bandval[6].'+</td>
                          
                    </tr>
             ';
              
              //get rooute first
            
              
             $aged_customers = get_salesman_customer_ids($slsmnid); 
              $outstanding_array = array(); 
              $Total_one = array();
              $Total_two = array();
              $Total_three = array();
              $Total_four = array();
              $Total_five = array();
              $Total_six = array();
              $Total_six_plus = array();
              foreach($aged_customers as $ageID){
                  $customerID =$ageID["CustomerID"];
                  $customerNm =get_customer_details($customerID);
                   $mycustName = $customerNm[0]["CustomerName"];
                   $CustomerNum = $customerNm[0]["CustomerNumber"];
                   $credit_limit = $customerNm[0]["CreditLimit"];
                   
                   if($mycustName==""){
                       $mycustName = "No Name";
                   }
                   if($credit_limit==0.00){
                       $credit_limit = "0.00";
                   }
              
              
                   echo     '<tr style="font-size:12px">
                             <td class="text-left">
                                <a class="font-"> '.$CustomerNum.'</a></td>
                              <td class="text-left">
                                <a class="font-">'.$mycustName.'</a></td>
                            <td class="text-right">
                                '.$credit_limit.'</td>';
                            
                            
                            $band_one = array();
                            $band_two = array();
                            $band_three = array();
                            $band_four = array();
                            $band_five = array();
                            $band_six = array();
                            $band_six_plus = array();
                            /**************AGEING IN RETROSPECT*******************************/
     $Data = last_date($customerID);
    $LastInsertDate =  $Data[0]["LastTransacationDate"];
    if($LastInsertDate>$date_to){
    $age_det = customer_aging($customerID,$date_to);
        @$trans_date = $age_det[0]["LastTransacationDate"];
        @$amount_owing = $age_det[0]["TotalOutstandingAmount"];
        @$transaction_type = $age_det[0]["TransactionType"];
         
        $diff = abs(strtotime($date_to) - strtotime($trans_date));
        $days = floor($diff / (60 * 60 * 24));
        if ($days >= 0 && $days <= $bandval[1]) {
            array_push($band_one, $amount_owing);
        } elseif ($days > $bandval[1] && $days <= $bandval[2]) {
            array_push($band_two, $amount_owing);
        } elseif ($days > $bandval[2] && $days <= $bandval[3]) {
            array_push($band_three, $amount_owing);
        } elseif ($days > $bandval[3] && $days <= $bandval[4]) {
            array_push($band_four, $amount_owing);
        } elseif ($days > $bandval[4] && $days <= $bandval[5]) {
            array_push($band_five, $amount_owing);
        } elseif ($days > $bandval[5] && $days <= $bandval[6]) {
            array_push($band_six, $amount_owing);
        } elseif ($days > $bandval[6]) {
            array_push($band_six_plus, $amount_owing);
        }
       
    }
    else{
       $age = CustomerAgingCurrent($customerID);
    foreach ($age as $age_det) {
        $trans_date = $age_det["LastTransacationDate"];
        $amount_owing = $age_det["TransactionAmount"];
        $transaction_type = $age_det["TransactionType"];
        if ($transaction_type == "Receipt" || $transaction_type == "Adjustment -") {
            $amount_owing = -$amount_owing;
        }
        $today = time();
        $diff = abs($today - strtotime($trans_date));
        $days = floor($diff / (60 * 60 * 24));
        if ($days >= 0 && $days <= $bandval[1]) {
            array_push($band_one, $amount_owing);
        } elseif ($days > $bandval[1] && $days <= $bandval[2]) {
            array_push($band_two, $amount_owing);
        } elseif ($days > $bandval[2] && $days <= $bandval[3]) {
            array_push($band_three, $amount_owing);
        } elseif ($days > $bandval[3] && $days <= $bandval[4]) {
            array_push($band_four, $amount_owing);
        } elseif ($days > $bandval[4] && $days <= $bandval[5]) {
            array_push($band_five, $amount_owing);
        } elseif ($days > $bandval[5] && $days <= $bandval[6]) {
            array_push($band_six, $amount_owing);
        } elseif ($days > $bandval[6]) {
            array_push($band_six_plus, $amount_owing);
        }
    }
    }
    
    /*************Date controller****************/
    
                            $totalOutstandingAmnt = array_sum($band_one)+array_sum($band_two)+array_sum($band_three)+array_sum($band_four)+array_sum($band_five)+array_sum($band_six)+array_sum($band_six_plus);
                            array_push($outstanding_array, $totalOutstandingAmnt);
                            
                    
                            array_push($Total_one, array_sum($band_one));
                            array_push($Total_two, array_sum($band_two));
                            array_push($Total_three, array_sum($band_three));
                            array_push($Total_four, array_sum($band_four));
                            array_push($Total_five, array_sum($band_five));
                            array_push($Total_six, array_sum($band_six));
                            array_push($Total_six_plus, array_sum($band_six_plus));
                            
                            
                           
                         
                           echo  '<td class="text-right" font size="2"><strong> '.$totalOutstandingAmnt.'</strong></td>
                            
                            <td class="text-right">'.array_sum($band_one).'</td>
                            
                               <td class="text-right">'.array_sum($band_two).'</td>
                         
                            <td class="text-right">'.array_sum($band_three).'</td>
                          
                            <td class="text-right">'.array_sum($band_four).'</td>
                            
                            <td class="text-right">'.array_sum($band_five).'</td>
                         
                            <td class="text-right">'.array_sum($band_six).'</td>
                          

                            <td class="text-right">'.array_sum($band_six_plus).'  </td>
                           
                        </tr>';
                 
              }
                             echo  '<tr style="font-size:13px">
                                    <td class="text-left" font size="2"><strong> </strong></td>
                                     <td class="text-left" font size="2"><strong>TOTALS </strong></td>
                             <td class="text-left" font size="2"><strong> </strong></td>
                             
                             <td class="text-right"> <strong>'.array_sum($outstanding_array).'</strong>  </td>
                            
                            <td class="text-right"> <strong>'.array_sum($Total_one).'</strong> </td>
                            
                            <td class="text-right"> <strong>'.array_sum($Total_two).'</strong></td>
                         
                            <td class="text-right"><strong>'.array_sum($Total_three).'</strong></td>
                          
                            <td class="text-right"><strong>'.array_sum($Total_four).'</strong></td>
                            
                            <td class="text-right"><strong>'.array_sum($Total_five).'</strong></td>
                         
                            <td class="text-right"><strong>'.array_sum($Total_six).'</strong></td>
                          

                            <td class="text-right"> <strong>'.array_sum($Total_six_plus).'</strong>   </td>
                            
                             </td>
                            
                        </tr>
                        
                       
                </tbody>
                </table> 
                
            </div>
  </div>
            <hr></hr>';
    
 
       }
       
       
       
       
        //end of for loop
