<?php

require '../DBAPI/dbapi.php';

$date_to = date("Y-m-d",strtotime($_POST["date_to"]));

if($date_to==date("Y-m-d")){
     $date_to = date("Y-m-d H:i:s");
 }
 elseif($date_to==date("1970-01-01")){
     $date_to = date("Y-m-d H:i:s");
 }

/* * *************************display starts here ******************* */







$band = $_POST["bands"];
//if routes are empty - get all routes or value = all

 echo '<div class="block-header table-responsive ">
      <table class="table table-vcenter col-xs-12 col-md-12 col-lg-12 mytable">
      <tr style="font-size:12px">
</tr>
      <tbody>
     <tr style="font-size:13px;">
     <td>
    <strong>Age Report as at <span class="badge badge-primary">'.$date_to.'</span><br>

</td></tr>   
</tbody>
<tr>
</tr>

</table>
</div>';

for ($i = 0; $i <= 7; $i++) {
    $bandval[$i] = $band * $i;
}





$route_name = get_routename(9);

 echo '<div class="block-header table-responsive ">
           
 <table class="table table-vcenter col-xs-12 col-md-12 col-lg-12 mytable" id="thistab">
 <tr>
</tr>
      <tbody>
     <tr style="font-size:12px">
     <td>
     <h6 ><strong>band : ' . $band . ' </strong></h3></div>
</td></tr>   
</tbody>
<tr>
</tr>

</table>
     </div>
 
           
  <div class="block-content">
          
            <div class="table-responsive ">
                <table class="table table-vcenter col-xs-12 col-md-12 col-lg-12">
           
                   <tbody>

                    <tr style="font-size:13px">
                         <td class="text-left " style="width:6%; text-transform: lowercase;"><strong>Acc #<strong></td>
                        <td class="text-left" style="width: 14%; text-transform: lowercase;"><strong>Customer</td>
                        <td class="text-right" style="width: 7%; text-transform: lowercase;"><strong>Cr Lim</td>
                        <td class=" text-right" style="width: 10%;text-transform: lowercase;"><strong>Balance</td>
                        <td class=" text-right" style="width: 9%;text-transform: lowercase;"><strong>' . $bandval[1] . ' Days</td>
                        <td  class=" text-right" style="width: 9%;text-transform: lowercase;" ><strong>' . $bandval[2] . ' Days</td>
                         <td class=" text-right" style="width: 9%;text-transform: lowercase;" ><strong>' . $bandval[3] . ' Days</td>
                        <td class="text-right" style="width: 9%;text-transform: lowercase;"><strong>' . $bandval[4] . ' Days</td>
                        <td class="text-right" style="width: 9%;text-transform: lowercase;"><strong>' . $bandval[5] . ' Days</td>
                        <td class="text-right" style="width: 9%;text-transform: lowercase;"><strong>' . $bandval[6] . ' Days</td>
                         <td class="text-right" style="width: 9%;text-transform: lowercase;"><strong>' . $bandval[6] . '+</td>
                          
                    </tr>

             ';

//get rooute first


$aged_customers = get_allaged_customer_IDs();
$outstanding_array = array();
$Total_one = array();
$Total_two = array();
$Total_three = array();
$Total_four = array();
$Total_five = array();
$Total_six = array();
$Total_six_plus = array();
foreach ($aged_customers as $ageID) 
    {
    $customerID = $ageID["CustomerID"];
    $customerNm = get_customer_details($customerID);
    $mycustName = $customerNm[0]["CustomerName"];
    $CustomerNum = $customerNm[0]["CustomerNumber"];
    $credit_limit = $customerNm[0]["CreditLimit"];

    if ($mycustName == "") {
        $mycustName = "No Name";
    }
    if ($credit_limit == 0.00) {
        $credit_limit = "0.00";
    }


    echo '<tr style="font-size:12px">
                             <td class="text-left">
                                <a class="font-"> ' . $CustomerNum . '</a></td>
                              <td class="text-left">
                                <a class="font-">' . $mycustName . '</a></td>
                            <td class="text-right">
                                ' . $credit_limit . '</td>';


    $band_one = array();
    $band_two = array();
    $band_three = array();
    $band_four = array();
    $band_five = array();
    $band_six = array();
    $band_six_plus = array();
    
    /**************AGEING IN RETROSPECT*******************************/
     $Data = last_date($customerID);
    $LastInsertDate =  $Data[0]["LastTransacationDate"];
    if($LastInsertDate>$date_to){
    $age_det = customer_aging($customerID,$date_to);
        @$trans_date = $age_det[0]["LastTransacationDate"];
        @$amount_owing = $age_det[0]["TotalOutstandingAmount"];
        @$transaction_type = $age_det[0]["TransactionType"];
         
        $diff = abs(strtotime($date_to) - strtotime($trans_date));
        $days = floor($diff / (60 * 60 * 24));
        if ($days >= 0 && $days <= $bandval[1]) {
            array_push($band_one, $amount_owing);
        } elseif ($days > $bandval[1] && $days <= $bandval[2]) {
            array_push($band_two, $amount_owing);
        } elseif ($days > $bandval[2] && $days <= $bandval[3]) {
            array_push($band_three, $amount_owing);
        } elseif ($days > $bandval[3] && $days <= $bandval[4]) {
            array_push($band_four, $amount_owing);
        } elseif ($days > $bandval[4] && $days <= $bandval[5]) {
            array_push($band_five, $amount_owing);
        } elseif ($days > $bandval[5] && $days <= $bandval[6]) {
            array_push($band_six, $amount_owing);
        } elseif ($days > $bandval[6]) {
            array_push($band_six_plus, $amount_owing);
        }
       
    }
    else{
       $age = CustomerAgingCurrent($customerID);
    foreach ($age as $age_det) {
        $trans_date = $age_det["LastTransacationDate"];
        $amount_owing = $age_det["TransactionAmount"];
        $transaction_type = $age_det["TransactionType"];
        if ($transaction_type == "Receipt" || $transaction_type == "Adjustment -") {
            $amount_owing = -$amount_owing;
        }
        $today = time();
        $diff = abs($today - strtotime($trans_date));
        $days = floor($diff / (60 * 60 * 24));
        if ($days >= 0 && $days <= $bandval[1]) {
            array_push($band_one, $amount_owing);
        } elseif ($days > $bandval[1] && $days <= $bandval[2]) {
            array_push($band_two, $amount_owing);
        } elseif ($days > $bandval[2] && $days <= $bandval[3]) {
            array_push($band_three, $amount_owing);
        } elseif ($days > $bandval[3] && $days <= $bandval[4]) {
            array_push($band_four, $amount_owing);
        } elseif ($days > $bandval[4] && $days <= $bandval[5]) {
            array_push($band_five, $amount_owing);
        } elseif ($days > $bandval[5] && $days <= $bandval[6]) {
            array_push($band_six, $amount_owing);
        } elseif ($days > $bandval[6]) {
            array_push($band_six_plus, $amount_owing);
        }
    }
    }
    
    /*************Date controller****************/
    
    $totalOutstandingAmnt = array_sum($band_one) + array_sum($band_two) + array_sum($band_three) + array_sum($band_four) + array_sum($band_five) + array_sum($band_six) + array_sum($band_six_plus);
    array_push($outstanding_array, $totalOutstandingAmnt);


    array_push($Total_one, array_sum($band_one));
    array_push($Total_two, array_sum($band_two));
    array_push($Total_three, array_sum($band_three));
    array_push($Total_four, array_sum($band_four));
    array_push($Total_five, array_sum($band_five));
    array_push($Total_six, array_sum($band_six));
    array_push($Total_six_plus, array_sum($band_six_plus));




    echo '<td class="text-right" font size="2"><strong> ' . $totalOutstandingAmnt . '</strong></td>
                            
                            <td class="text-right">' . array_sum($band_one) . '</td>
                            
                               <td class="text-right">' . array_sum($band_two) . '</td>
                         
                            <td class="text-right">' . array_sum($band_three) . '</td>
                          
                            <td class="text-right">' . array_sum($band_four) . '</td>
                            
                            <td class="text-right">' . array_sum($band_five) . '</td>
                         
                            <td class="text-right">' . array_sum($band_six) . '</td>
                          

                            <td class="text-right">' . array_sum($band_six_plus) . '  </td>
                           
                        </tr>';
}
echo '<tr style="font-size:13px">
                                    <td class="text-left" font size="2"><strong> </strong></td>
                                     <td class="text-left" font size="2"><strong>TOTALS </strong></td>
                             <td class="text-left" font size="2"><strong> </strong></td>
                             
                             <td class="text-right"> <strong>' . array_sum($outstanding_array) . '</strong>  </td>
                            
                            <td class="text-right"> <strong>' . array_sum($Total_one) . '</strong> </td>
                            
                            <td class="text-right"> <strong>' . array_sum($Total_two) . '</strong></td>
                         
                            <td class="text-right"><strong>' . array_sum($Total_three) . '</strong></td>
                          
                            <td class="text-right"><strong>' . array_sum($Total_four) . '</strong></td>
                            
                            <td class="text-right"><strong>' . array_sum($Total_five) . '</strong></td>
                         
                            <td class="text-right"><strong>' . array_sum($Total_six) . '</strong></td>
                          

                            <td class="text-right"> <strong>' . array_sum($Total_six_plus) . '</strong>   </td>
                            
                             </td>
                            
                        </tr>
                        
                       
                </tbody>
                </table> 
                
            </div>
  </div>
            <hr></hr>';







//end of for loop
