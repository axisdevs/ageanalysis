<?php require 'inc/config.php'; ?>
<?php //require 'inc/views/template_head_start.php';  ?>
<?php require 'inc/views/template_head_end.php'; ?>


<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2-bootstrap.min.css">
<!-- Page Content -->
<div class="col-sm-offset-2 col-md-offset-2 col-sm-6 col-md-6 block hidden-print">
    <div class="block-content block-content-full">
        <div class="form-group">
              <div class="col-xs-3">
                   <div class="form-material">
                      <button class="btn btn-sm btn-default btn_by_route"  type="submit">Age By Route </button>
                  </div>
              </div>

                  <div class="col-xs-3">
                  <div class="form-material">
                      <button class="btn btn-sm btn-default btn_by_salesman " type="submit">Age By Salesman</button>
                  </div>
              </div> 

            <div class="col-xs-3">
                <div class="form-material">
                    <button class="btn btn-sm btn-default btn_by_days " type="submit">Age Split By Days </button>
                </div>
            </div>

            <div class="col-xs-3">
                <div class="form-material">
                    <button class="btn btn-sm btn-danger btn_cntrl_exceps " type="submit">Handle Imbalances </button>
                </div>
            </div>

        </div>
    </div>

</div>
<div class="content content-boxed ult_content">
    <!-- if report type is customer-->

    <div class="row hidden-print byroute">

        <div class="col-sm-12 col-md-12 block">
            <div class="block-content block-content-full">
                <div class="form-group">
                    <div class="col-xs-3 col-md-3 col-lg-3 col-sm-3">
                        <div class="form-material">
                            <select class="js-select2 form-control" id="route" name="route" style="width: 100%;" data-placeholder="Choose Route.." multiple>
                                <option value="ALL">Select All</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                <?php
                                $routes = get_routes();
                                foreach ($routes as $myrut) {
                                    ?>
                                    <option value="<?php echo $myrut['RouteID']; ?>"><?php echo get_routename($myrut['RouteID']); ?></option>
                                <?php } ?>
                            </select>
                            <label for="example2-select2-multiple">Routes</label>
                        </div>
                    </div>

                    <div class="col-xs-3 col-md-3 col-lg-3 col-sm-3">
                        <div class="form-material">
                            <select class="js-select2 form-control" id="rt_bands" name="bands" style="width: 100%;" data-placeholder="Choose custom bands..." >
                                <option value="7">7 Days</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                <option value="15">15 Days</option>
                                <option value="30">30 Days</option>


                            </select>
                            <label for="example2-select2-multiple">Bands</label>
                        </div>
                    </div>

                    <div class="col-xs-3 col-md-3 col-lg-3 col-sm-3">
                        <div class="form-material">
                            <div class="input-daterange input-group " data-date-format="mm/dd/yyyy">

                                <input class="form-control" type="text" id="date_to" name="example-daterange2" placeholder="To Date">
                            </div>
                            <label for="example-daterange1">As At Date</label>
                        </div>
                    </div>


                    <div class="col-xs-3 col-md-3 col-lg-3 col-sm-3">
                        <div class="form-material">
                            <button class="btn btn-sm btn-primary btn_rt_report" type="submit">View Report </button>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <!-- if report type is salesman-->
    <div class="row hidden-print bysalesman">

        <div class="col-sm-12 col-md-12 block">
            <div class="block-content block-content-full">
                <div class="form-group">
                    <div class="col-xs-3">
                        <div class="form-material">
                            <select class="js-select2 form-control" id="salesman" name="salesman" style="width: 100%;" data-placeholder="Choose salesman..." multiple>
                                <option value="ALL">Select All</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                <?php
                                $salesman = get_salesman();
                                foreach ($salesman as $persons) {
                                    ?>
                                    <option value="<?php echo $persons['UserID']; ?>"><?php echo $persons['Username']; ?></option>
                                <?php } ?>
                            </select>
                            <label for="example2-select2-multiple">Salesman</label>
                        </div>
                    </div>

                    <div class="col-xs-3">
                        <div class="form-material">
                            <select class="js-select2 form-control" id="salesmnbands" name="bands" style="width: 100%;" data-placeholder="Choose custom bands..." >
                                <option value="7">7 Days</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                <option value="15">15 Days</option>
                                <option value="30">30 Days</option>


                            </select>
                            <label for="example2-select2-multiple">Bands</label>
                        </div>
                    </div>

                    <div class="col-xs-3">
                        <div class="form-material">
                            <div class="input-daterange input-group " data-date-format="mm/dd/yyyy">
                                <input class="form-control" type="text" id="salesman_date_to" name="example-daterange2" placeholder="To Date">
                            </div>
                            <label for="example-daterange1">As At Date</label>
                        </div>
                    </div>


                    <div class="col-xs-3">
                        <div class="form-material">
                            <button class="btn btn-sm btn-primary btn_age_salesman" type="submit">View Report </button>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>   


    <!-- if report type is by customer by route-->
    <div class="row hidden-print general_age">

        <div class="col-sm-12 col-md-12 block">
            <div class="block-content block-content-full">
                <div class="form-group">
                    <div class="col-xs-4">
                        <div class="form-material">
                            <select class="js-select2 form-control" id="bands" name="bands" style="width: 100%;" data-placeholder="Choose custom bands..." >
                                <option value="7">7 Days</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                <option value="15">15 Days</option>
                                <option value="30">30 Days</option>


                            </select>
                            <label for="example2-select2-multiple">Bands</label>
                        </div>
                    </div>

                    <div class="col-xs-4">
                        <div class="form-material">
                            <div class="input-daterange input-group " data-date-format="mm/dd/yyyy">
                                <input class="form-control" type="text" id="band_date_to" name="example-daterange2" placeholder="To Date">
                            </div>
                            <label for="example-daterange1">As At Date</label>
                        </div>
                    </div>


                    <div class="col-xs-4">
                        <div class="form-material">
                            <button class="btn btn-sm btn-primary btn_bands_age" type="submit">View Report </button>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>   

    <div class="row hidden-print excep">

        <div class="col-sm-12 col-md-12 block">
            <div class="block-content block-content-full">
                <div class="form-group">
                    <div class="col-xs-4">
                        <div class="form-material">
                            <select class="js-select2 form-control" id="opts" name="opts" style="width: 100%;" data-placeholder="Choose Customer with imbalances..." >
                                <option value=""> select options</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                               <option value="list"> Select from List</option>
                               <option value="comma"> Paste Comma Values</option>
                            </select>
                            <label for="example2-select2-multiple">Pick Option</label>
                        </div>
                    </div>
                    <div class="col-xs-4 array_custs">
                        <div class="form-material">
                            <select class="js-select2 form-control custs" id="custs" name="custs" style="width: 100%;" multiple data-placeholder="Choose Customer with imbalances..." >
                                <option value="">Select Customer Numbers</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                <?php $Custs = getCustNum();
                                foreach($Custs as $dt){
                                    $CustNum = $dt["CustomerNumber"]; ?>
                                <option value="<?php echo $CustNum; ?>"><?php echo $CustNum; ?></option>
                                <?php } ?>
                            </select>
                            <label for="example2-select2-multiple">Customers</label>
                        </div>
                    </div>
                    
                    <div class="col-xs-4 noaray_custs">
                        <div class="form-material">
                            <input type="text" class="form-control custs" id="lists" name="custs" placeholder="paste comma seperated values here">
                            <label for="example2-select2-multiple">Customers</label>
                        </div>
                    </div>
                    
                       <div class="col-xs-4">
                        <div class="form-material">
                            <button class="btn btn-sm btn-primary btn_crct_excp" type="submit">Submit Changes </button>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div> 



    <!-- Block Tabs Justified Default Style -->
    <div class="block">

        <div class="block-content tab-content">
            <div class="tab-pane active" id="btabs-static-justified-html">

                <button type="button" class="hidden-print print_btn pull-right" onclick="App.initHelper('print-page');"><i class="si si-printer"></i> Print Report</button>     <!-- Full Table -->
                <button type="button" class="hidden-print print_btn pull-right" onClick ="$('.report_contents').tableExport({type: 'excel', escape: 'false'});">Print to Excel</button>  


                <div class="report_contents col-xs-12 ">

                </div>
            </div>


        </div>
    </div>
    <!-- END Block Tabs Justified Alternative Style -->

</div>
<!-- END Page Content -->


<?php require 'inc/views/template_footer_start.php'; ?> 
<script src="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2.full.min.js"></script>
<script src="<?php echo $one->assets_folder; ?>/js/plugins/prints/jspdf.debug.js"></script>
<script src="<?php echo $one->assets_folder; ?>/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?php echo $one->assets_folder; ?>/js/plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="<?php echo $one->assets_folder; ?>/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="<?php echo $one->assets_folder; ?>/exportpages/tableExport.js"></script>
<script type="text/javascript" src="<?php echo $one->assets_folder; ?>/exportpages/jquery.base64.js"></script>
<!-- Page JS Code -->
<script>
                    jQuery(function () {
                        // Init page helpers (Appear + CountTo plugins)
                        App.initHelpers(['appear', 'appear-countTo', 'select2', 'colorpicker', 'datepicker', 'datetimepicker']);
                    });

                    $(document).ready(function () {
                        $(".byroute").hide();
                        $(".bysalesman").hide();
                        $(".general_age").hide();
                        $(".ult_content").hide();

                        $('.print_btn').hide();
                        $(".array_custs").hide();
                        $(".noaray_custs").hide();
                        
                        $("#opts").on("change",function(){
                            if($(this).val()==="list"){
                                $(".array_custs").show("slow");
                        $(".noaray_custs").hide();
                            }else{
                                $(".array_custs").hide("slow");
                        $(".noaray_custs").show("slow");
                            }
                        });
                        

                        // click to activate route 
                        $(".btn_by_route").click(function (e) {
                            e.preventDefault();
                            $(".ult_content").show('1000');
                            $(".byroute").show('1000');
                            $(".bysalesman").hide('slow');
                            $(".general_age").hide('slow');
                            $(".excep").hide('slow');
                        });

                        // click to activate salesman 
                        $(".btn_by_salesman").click(function (e) {
                            e.preventDefault();
                            $(".ult_content").show('1000');
                            $(".byroute").hide('slow');
                            $(".bysalesman").show('1000');
                            $(".general_age").hide('slow');
                            $(".excep").hide('slow');
                        });

                        // click to activate salesman 
                        $(".btn_by_days").click(function (e) {
                            e.preventDefault();
                            $(".ult_content").show('1000');
                            $(".byroute").hide('slow');
                            $(".bysalesman").hide('slow');
                            $(".general_age").show('1000');
                            $(".excep").hide('slow');
                        });

                        $(".btn_cntrl_exceps").click(function (e) {
                            e.preventDefault();
                            $(".excep").show('1000');
                            $(".ult_content").show('slow');
                            $(".byroute").hide('slow');
                            $(".bysalesman").hide('slow');
                            $(".general_age").hide('slow');
                        });



                        //age by route 
                        $(".btn_rt_report").click(function () {
                            $(".report_contents").html("........Please wait while system processes age report by route.............");
                            $(this).prop("disabled", true);
                            $.post('report_engines/age_by_route.php', {
                                bands: $("#rt_bands").val(),
                                routes: $("#route").val(),
                                date_from: $("#date_from").val(),
                                date_to: $("#date_to").val()

                            },
                                    function (response) {
                                        $('.print_btn').show(1000);
                                        $(".report_contents").html(response);
                                        $(".btn_rt_report").prop("disabled", false);
                                    });
                        });

                        //age by salesman
                        $(".btn_age_salesman").click(function () {
                            $(this).prop("disabled", true);
                            $(".report_contents").html("........Please wait while system processes age report by salesman.............");
                            $.post('report_engines/age_by_salesman.php', {
                                bands: $("#salesmnbands").val(),
                                salesman: $("#salesman").val(),
                                date_from: $("#salesman_date_from").val(),
                                date_to: $("#salesman_date_to").val()

                            },
                                    function (response) {
                                        $('.print_btn').show(1000);
                                        $(".btn_age_salesman").prop("disabled", false);
                                        $(".report_contents").html(response);

                                    });
                        });


                        //age by bands
                        $(".btn_bands_age").click(function () {
                            $(this).prop("disabled", true);
                            $(".report_contents").html("........Please wait while system processes the overall age report.............");

                            $.post('report_engines/days_split.php', {
                                bands: $("#bands").val(),
                                date_from: $("#band_date_from").val(),
                                date_to: $("#band_date_to").val()

                            },
                                    function (response) {
                                        $('.print_btn').show(1000);
                                        $(".btn_bands_age").prop("disabled", false);
                                        $(".report_contents").html(response);

                                    });
                        });
                        
                         $(".btn_crct_excp").click(function () {
                            $(this).prop("disabled", true);
                            var opt =  $("#opts").val();
                            var customerDt;
                            if(opt==="list"){
                                customerDt = $("#custs").val();
                            }else{
                                 customerDt = $("#lists").val();
                            }
                            $(".report_contents").html("........Handling Exceptions, Please wait.............");

                            $.post('DBAPI/Remedy.php', {
                                custs: customerDt
                               

                            },
                                    function (response) {
                                       // $('.print_btn').show(1000);
                                        $(".btn_crct_excp").prop("disabled", false);
                                        $(".report_contents").html(response);

                                    });
                        });




                    });

</script>

